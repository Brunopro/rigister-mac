import { Component } from '@angular/core';

import { PreferencesProvider } from '../../providers/preferences';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {


  //injetable o arquivo preferences
  constructor(public preferences: PreferencesProvider ) { 
    //acho q não precisa desse código
   // this.preferences.get().then(user => console.log(user))
  }

  

}
