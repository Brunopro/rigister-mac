import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { User } from '../../models/user';
import { PreferencesProvider } from '../../providers/preferences';
import { AuthProvider } from '../../providers/auth/auth';
import { LoginPage } from '../login/login';
import { App } from 'ionic-angular';
 

@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html',
})
export class PerfilPage {

  public user = {} as User;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public preference: PreferencesProvider, 
    public auth: AuthProvider,
    public app: App, 
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PerfilPage');
  }

  logout(){
    
    this.preference.remove()
    this.auth.logout()   
    this.app.getRootNav().setRoot(LoginPage);
      
  }
}
